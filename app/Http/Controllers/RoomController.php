<?php namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Securimage;
use Sentinel;
use App\Reservation;
use App\Rooms;
use View;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Response;
class Room {}
class RoomController extends Controller {



	/**
	 * Initializer.
	 *
	 * @return void
	 */
	public function __construct()
	{


	}

	public function getRooms(Request $request){
		$capacity = $request->get('capacity', 0);
		$rooms = array();
		if($capacity != 0) {
			$rooms = DB::table('rooms')->where('capacity', '=', $capacity)->orderby('name', 'asc')->get();

		}else {
			$rooms = DB::table('rooms')->orderby('name', 'asc')->get();

		}

		$result = array();

		foreach($rooms as $room) {
			$r = new Room();
			$r->id = $room->id;
			$r->name = $room->name;
			$r->capacity = $room->capacity;
			$r->status = $room->status;
			$result[] = $r;

		}
		return Response::json($result);
	}
}