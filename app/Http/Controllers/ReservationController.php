<?php namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\MessageBag;
use Securimage;
use Sentinel;
use App\Reservation;
use App\Rooms;
use View;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
class Result {}
class ReservationController extends Controller {



	/**
	 * Initializer.
	 *
	 * @return void
	 */
	public function __construct()
	{


	}
	public function getRoomPlan(Request $request, $startdate1 = 0, $enddate1 = 0, $moveval = 0){

		$defaultdate = $startdate1;
		if($startdate1 == 0){
			$startdate1 = date('Y-m-d', time());
			$defaultdate = date('Y-m-d', time());
		}
		if($enddate1 == 0){
			$enddate1 = date('Y-m-d', time());
			$enddate1 = date('Y-m-d', strtotime($enddate1.'+1 months'));
		}
		if($moveval != 0){
			$startdate1= date('Y-m-d', strtotime($startdate1. "".$moveval." days"));
			$defaultdate = $startdate1;
			$enddate1= date('Y-m-d', strtotime($enddate1. "".$moveval." days"));

		}

		$moveval = 0;
		return View('admin.pms.roomplan', compact('startdate1', 'enddate1', 'defaultdate', 'moveval'));
	}
	public function  getChangeRoomPlan(Request $request){
		$reservation_id = $request->get('reservationid', 0);
		$reservation_name = $request->get('reservationname', 0);
		$room_id = $request->get('roomid', 0);
		$startdate = $request->get('startdate', '');
		$enddate = $request->get('enddate', '');
		$temp = explode('/', $startdate);
		if(count($temp) > 1){
			$date_aux = date_create_from_format('Y/m/d', $startdate);
			$startdate =  date_format($date_aux,'Y-m-d');
		}
		$temp = explode('/', $enddate);
		if(count($temp) > 1){
			$date_aux = date_create_from_format('Y/m/d', $enddate);
			$enddate =  date_format($date_aux,'Y-m-d');
		}

		$reservation = DB::table('reservations')->where('room_id', $room_id)->where('id', '!=', $reservation_id)
			->where(function($query) use($startdate, $enddate){
				$query->where(function($query1) use( $startdate, $enddate){
					$query1->where('start', '>=', $startdate);
					$query1->where('start', '<=', $enddate);
				});
				$query->orWhere(function($query1) use( $startdate, $enddate){
					$query1->where('end', '>=', $startdate);
					$query1->where('end', '<=', $enddate);
				});
			})
			->first();
		$msg = '';
		$msg_type = '';
		if(empty($reservation)) {
			$reservation = DB::table('reservations')->where('id', $reservation_id)->first();

			if (empty($reservation)) {
				DB::table('reservations')->insert(['name' => $reservation_name, 'room_id' => $room_id, 'start' => $startdate, 'end' => $enddate, 'status' => 1, 'paid' => 0]);
			} else {
				DB::table('reservations')->where('id', $reservation_id)->update(['name' => $reservation_name, 'room_id' => $room_id, 'start' => $startdate, 'end' => $enddate, 'status' => 1]);
			}
			$msg = 'Successfully reservated!';
			$msg_type = 'success';
		}else{
			$msg = 'it has already reservated dates on selected room.';
			$msg_type = 'error';
		}

		return Redirect::back()->with('msg1', $msg)->with('msg_type', $msg_type);
	}













	public function moveReservation(Request $request){
		$newstart = $request->get('newStart', 0);
		$newEnd = $request->get('newEnd', 0);
		$id = $request->get('id', 0);
		$newResource = $request->get('newResource', 0);
		//SELECT * FROM reservations WHERE NOT ((end <= :start) OR (start >= :end)) AND id <> :id AND room_id = :resource
		$query = 'SELECT * FROM reservations WHERE NOT ((end <= '.$newstart.') OR (start >= '.$newEnd.')) AND id <> '.$id.' AND room_id = '.$newResource;
		$reservation = DB::select($query);
		if(!empty($reservation)){
			$response = new Result();
			$response->result = 'Error';
			$response->message = 'This reservation overlaps with an existing reservation.';
			header('Content-Type: application/json');
			return json_encode($response);
		}

		$query = 'UPDATE reservations SET start = '.$newstart.', end = '.$newEnd.', room_id = '.$newResource.' WHERE id = '.$id;
		$reservation = DB::table('reservations')->where('id', $id)->update(['start'=>$newstart, 'end'=>$newEnd, 'room_id'=>$newResource]);
		$response = new Result();
		$response->result = 'OK';
		$response->message = 'Update successful';

		header('Content-Type: application/json');
		return json_encode($response);
	}

	public function resizeReservation(Request $request){
		$newstart = $request->get('newStart', 0);
		$newEnd = $request->get('newEnd', 0);
		$id = $request->get('id', 0);

		$reservation = DB::table('reservations')->where('id', $id)->update(['start'=>$newstart, 'end'=>$newEnd]);

		$response = new Result();
		$response->result = 'OK';
		$response->message = 'Update successful';

		header('Content-Type: application/json');
		return json_encode($response);
	}
	public function deleteReservation(Request $request){
		$newstart = $request->get('newStart', 0);
		$newEnd = $request->get('newEnd', 0);
		$id = $request->get('id', 0);


		DB::table('reservations')->where('id', $id)->delete();


		$response = new Result();
		$response->result = 'OK';
		$response->message = 'Delete successful';

		header('Content-Type: application/json');
		return json_encode($response);
	}
	public function editReservation(Request $request, $id = 0){
		$reservation = DB::table('reservations')->where('id', $id)->first();
		$rooms = DB::table('rooms')->orderby('id', 'asc')->get();
		return View('admin.reservationedit', compact('id', 'reservation', 'rooms'));
	}
	public function newReservation(Request $request, $id = 0){
		$reservation = DB::table('reservations')->first();
		$rooms = DB::table('rooms')->orderby('id', 'asc')->get();
		return View('admin.reservationnew', compact('id', 'reservation', 'rooms'));
	}
	public function getReservation(Request $request){
		$newstart = $request->get('newStart', 0);
		$newEnd = $request->get('newEnd', 0);
		$reservations = DB::table('reservations')->where('end', '>=', $newEnd)->where('start', '<=', $newstart)->get();
		$events = array();

		date_default_timezone_set("UTC");
		$now = Carbon::now();
		$today = $now->setTime(0, 0, 0);

		foreach($reservations as $row) {
			$e = new Event();
			$e->id = $row->id;
			$e->text = $row->name;
			$e->start = $row->start;
			$e->end = $row->end;
			$e->resource = $row->room_id;
			$e->bubbleHtml = 'Reservation details: <br/>'.$row->name;

			// additional properties
			$e->status = $row->status;
			$e->paid = $row->paid;
			$events[] = $e;

			/*
                int paid = Convert.ToInt32(e.DataItem["ReservationPaid"]);
                string paidColor = "#aaaaaa";

                e.Areas.Add(new Area().Bottom(10).Right(4).Html("<div style='color:" + paidColor + "; font-size: 8pt;'>Paid: " + paid + "%</div>").Visibility(AreaVisibility.Visible));
                e.Areas.Add(new Area().Left(4).Bottom(8).Right(4).Height(2).Html("<div style='background-color:" + paidColor + "; height: 100%; width:" + paid + "%'></div>").Visibility(AreaVisibility.Visible));
             * */
		}

		header('Content-Type: application/json');
		return json_encode($events);
	}
	public function updateReservation(Request $request){
		$id = $request->get('id', 0);
		$name = $request->get('name', '');
		$end = $request->get('end', '');
		$start = $request->get('start', '');
		$room = $request->get('room', 0);
		$status = $request->get('status', 0);
		$paid = $request->get('paid', 0);
		$reservation = DB::table('reservations')->where('id', $id)->update(['name'=>$name, 'start'=>$start, 'end'=>$end, 'room_id'=>$room, 'status'=>$status, 'paid'=>$paid]);
		$response = new Result();
		$response->result = 'OK';
		$response->message = 'Update successful';

		header('Content-Type: application/json');
		echo json_encode($response);
	}
	public function addReservation(Request $request){
		$id = $request->get('id', 0);
		$name = $request->get('name', '');
		$end = $request->get('end', '');
		$start = $request->get('start', '');
		$room = $request->get('room', 0);
		$status = $request->get('status', 0);
		$paid = $request->get('paid', 0);
		$reservation = DB::table('reservations')->where('id', $id)->update(['name'=>$name, 'start'=>$start, 'end'=>$end, 'room_id'=>$room, 'status'=>$status, 'paid'=>$paid]);
		$response = new Result();
		$response->result = 'OK';
		$response->message = 'Update successful';

		header('Content-Type: application/json');
		echo json_encode($response);
	}
}