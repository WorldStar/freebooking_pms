
                <section class="content">
                    <script src="{{ url('js/html5shiv.js') }}" type="text/javascript"></script>
                    <script src="{{ url('js/respond.min.js') }}" type="text/javascript"></script>
                    <script src="{{ asset('assets/js/jquery.min.js') }}" type="text/javascript"></script>
                    <script src="{{ asset('assets/js/jquery-ui.min.js') }}" type="text/javascript"></script>
                    <script src="{{ url('js/daypilot/daypilot-all.min.js') }}" type="text/javascript"></script>
                    <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet" type="text/css"/>
                    <script src="{{ asset('assets/js/app.js') }}" type="text/javascript"></script>
                    <script>
                        /* $.ajaxSetup({
                         headers: {
                         'X-CSRF-TOKEN': "{{ csrf_token() }}"
                         }
                         }); */

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                    </script>
                    <?php
                    // check the input
                    is_numeric($id) or die("invalid URL");

                    ?>
                    <form id="f" action="backend_update.php" style="padding:20px;">
                        <input type="hidden" name="id" value="<?php print $id ?>" />
                        <h1>Edit Reservation</h1>
                        <div>Start:</div>
                        <div><input type="text" id="start" name="start" value="<?php print $reservation->start; ?>" /></div>
                        <div>End:</div>
                        <div><input type="text" id="end" name="end" value="<?php print $reservation->end ?>" /></div>
                        <div>Room:</div>
                        <div>
                            <select id="room" name="room">
                                <?php
                                foreach ($rooms as $room) {
                                    $selected = $reservation->room_id == $room->id ? ' selected="selected"' : '';
                                    $id1 = $room->id;
                                    $name = $room->name;
                                    print "<option value='$id1' $selected>$name</option>";
                                }
                                ?>
                            </select>

                        </div>
                        <div>Name: </div>
                        <div><input type="text" id="name" name="name" value="<?php print $reservation->name ?>" /></div>
                        <div>Status:</div>
                        <div>
                            <select id="status" name="status">
                                <?php
                                $options = array("New", "Confirmed", "Arrived", "CheckedOut");
                                foreach ($options as $option) {
                                    $selected = $option == $reservation->status ? ' selected="selected"' : '';
                                    $id1 = $option;
                                    $name = $option;
                                    print "<option value='$id1' $selected>$name</option>";
                                }
                                ?>
                            </select>
                        </div>
                        <div>Paid:</div>
                        <div>
                            <select id="paid" name="paid">
                                <?php
                                $options = array(0, 50, 100);
                                foreach ($options as $option) {
                                    $selected = $option == $reservation->paid ? ' selected="selected"' : '';
                                    $id1 = $option;
                                    $name = $option."%";
                                    print "<option value='$id1' $selected>$name</option>";
                                }
                                ?>
                            </select>

                        </div>

                        <div class="space"><input type="submit" value="Save" /> <a href="javascript:close();">Cancel</a></div>
                    </form>

                    <script type="text/javascript">
                        function close(result) {
                            if (parent && parent.DayPilot && parent.DayPilot.ModalStatic) {
                                parent.DayPilot.ModalStatic.close(result);
                            }
                        }

                        $("#f").submit(function () {
                            var f = $("#f");
                            $.post(f.attr("action"), f.serialize(), function (result) {
                                close(eval(result));
                            });
                            return false;
                        });

                        $(document).ready(function () {
                            $("#name").focus();
                        });

                    </script>
                </section>
