@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Room Plans
@parent
@stop

{{-- page level styles --}}
@section('header_styles')

<!--page level styles ends-->
<link href="/public/assets/vendors/daterangepicker/css/daterangepicker.css" rel="stylesheet" type="text/css" />
<link href="/public/assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<link href="/public/assets/vendors/clockface/css/clockface.css" rel="stylesheet" type="text/css"/>
<link href="/public/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="/public/assets/css/pages/buttons.css" />
@stop
{{-- Page content --}}
@section('content')

<section class="content-header" style="margin-bottom: 0px !important;">
    <div style="float: left;margin-right: 10px;"><h1 style="vertical-align: top;margin-top: -1px;">Room Plan - </h1></div>
    <div class="input-group">
        <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
        </div>
        <input type="text" class="form-control" id="daterange1" style="width:300px;"/>
    </div>
</section>
<section class="content-header" style="margin-bottom: 0px !important;min-height: 74px;">
    <div style="float:left;width:220px;">
        <div style="margin-right: 10px;"><span style="font-size: 15px;">Select Day</span></div>
        <div class="input-group">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control" id="rangepicker4" style="width:200px;"/>
        </div>
    </div>
    <div style="float:right;">
        <div class="tagright" style="float: right;">
            <div style="margin-right: 0px;font-size: 15px;text-align: right;">Right Tag</div>
            <div class="input-group">
                <button type="button" class="btn btn-responsive button-alignment btn-success" style="margin-bottom:7px;" data-toggle="button">+1 ></button>&nbsp;
                <button type="button" class="btn btn-responsive button-alignment btn-success" style="margin-bottom:7px;" data-toggle="button">+7 >></button>&nbsp;
                <button type="button" class="btn btn-responsive button-alignment btn-success" style="margin-bottom:7px;" data-toggle="button">+21 >></button>
            </div>
        </div>
        <div class="tagcenter" style="float: right;">
            <div style="margin-right: 10px;"><span style="font-size: 15px;">&nbsp;</span></div>
            <div class="input-group">
                <button type="button" class="btn btn-responsive button-alignment btn-warning" style="margin-bottom:7px;" data-toggle="button">Day</button>&nbsp;
            </div>
        </div>
        <div class="tagleft" style="float: right;">
            <div style="margin-right: 10px;"><span style="font-size: 15px;">Left Tag</span></div>
            <div class="input-group">
                <button type="button" class="btn btn-responsive button-alignment btn-primary" style="margin-bottom:7px;" data-toggle="button"><< -21</button>&nbsp;
                <button type="button" class="btn btn-responsive button-alignment btn-primary" style="margin-bottom:7px;" data-toggle="button"><< -7</button>&nbsp;
                <button type="button" class="btn btn-responsive button-alignment btn-primary" style="margin-bottom:7px;" data-toggle="button">< -1</button>&nbsp;
            </div>
        </div>

    </div>
</section>
<!-- Main content -->
<section class="content">
    <div id="header">
        <div class="bg-help">
            <div class="inBox">
                <hr class="hidden" />
                <link rel="stylesheet" type="text/css" href="/public/js/druid/base.css" media="all">
                <link rel="stylesheet" type="text/css" href="/public/js/druid/inc/stylesheet.css" media="all">
                <link rel="stylesheet" type="text/css" href="/public/js/druid/inc/screen.css" media="screen">
                <link rel="stylesheet" type="text/css" href="/public/js/druid/inc/paper.css" media="print">
                <script type="text/javascript" src="/public/js/druid/inc/functions.js">
                </script>
                <script type="text/javascript" src="/public/js/druid/base.js"></script>
                <span id="m_corr_su"></span>

                <div style="position: relative;"  ondragover="event.preventDefault();" ondragenter="event.preventDefault();" ondrop="event.preventDefault();drp_out();">
                    <span id="m_corr_su"></span>
                    <script type="text/javascript">
                        <!--
                        var colore_date_norm = '#daedff';
                        var colore_date_altre = '#b7dcff';
                        var colore_date_sel = '#ffffff';
                        var colore_drp1 = '#05e105';
                        var colore_drp2 = '#297929';

                        var id_ini_tab = 182;
                        var id_fine_tab = 213;
                        var agg_tronca = -2;
                        var riduci_font = 0;
                        var tipo_periodi = 'g';
                        var id_sessione = '';
                        var d = new Date();
                        var anno = d.getFullYear();
                        var allinea_tab_mesi = 'NO';
                        var tab_spostata = 0;
                        var sel_start_date = 0;
                        var sel_start_col = 0;
                        var sel_stop_date = 0;
                        var curr_sel_row = 0;
                        -->
                    </script>



                    <div style="position: relative; margin-left: 2%; margin-top: 10px" ondragstart="drg(event)">
                        <?php
                        $currentdate = date('d/m/Y', time());
                        $enddate = date('d/m/Y', strtotime('+1 months'));


                        $date_aux = date_create_from_format('d/m/Y', $currentdate);
                        $date =  date_format($date_aux,'Y-m-d');
                        $now = strtotime($date);

                        $date_aux = date_create_from_format('d/m/Y', $enddate);
                        $date =  date_format($date_aux,'Y-m-d');
                        $your_date = strtotime($date);

                        $datediff = $your_date - $now;

                        $days = floor($datediff/(60*60*24)) + 2;


                        $rooms = DB::table('rooms')->orderby('id', 'asc')->get();
                        $book3 = array('3', '15', '5'); //room number, start date, date length
                        $book4 = array('5', '22', '6');

                        $topbars = array();
                        $dayarray = array();
                        $week = array('Su', 'Mon', 'Tu', 'We', 'Th', 'Fr', 'Sa');
                        for($i = 1; $i < $days; $i++){
                            $j = $i-1;
                            $NewDate= date('y-m-d', strtotime("+".$j." days"));
                            $d1= date('d', strtotime("+".$j." days"));
                            $m1= date('w', strtotime("+".$j." days"));
                            $topbars[$i] = array($d1, $week[$m1]);
                            $dayarray[$i] = $NewDate;

                        }
                        $width = 93 / $days;
                        $fwidth = 100 - $days *$width;
                        $val = '<table class="m1" style="background-color: #b9ccd4;width:96%;" cellpadding="3" ><tbody>';
                        $val .= '<tr class="rd_r" style="background-color: #daedff;">';
                        $val .= '<td style="background-color: #b9ccd4; padding: 0 2px 0 2px;width:'.$fwidth.'%"><a name="rd_n1"></a>Rooms</td>';

                        $jsArDaCo = array();
                        for($i = 1; $i < $days; $i++){
                            if($topbars[$i][1] == 'Su'){
                                $val .= '<td class="rd_'.$i.'" colspan="2"><b style="color: red;">'.$topbars[$i][0].'<small><br>'.$topbars[$i][1].'</small></b></td>';
                            }else{
                                $val .= '<td class="rd_'.$i.'" colspan="2">'.$topbars[$i][0].'<small><br>'.$topbars[$i][1].'</small></td>';
                            }
                            $k = $i -1;
                            $d1= date('Y-m-d', strtotime("+".$k." days"));
                            $jsArDaCo[] = '"'.$d1.'"';
                        }
                        $val .= '</tr>';
                        $extra = 0;

                        $i = 0;
                        $jsrooms = array();
                        $jsroomnos = array();
                        $jsreservations = array();
                        $jsreservationids = array();
                        $jsApAs = array();
                        foreach($rooms as $room){
                            $jsrooms[] = '"'.$room->name.'"';
                            $jsroomnos[] = '"'.$room->id.'"';
                            $jsApAs[] = '""';
                        }
                        foreach($rooms as $room){
                            $i++;
                            $roomcolor = DB::table('room_types')->where('id', $room->type_id)->first()->color;
                            $val .= '<tr id="app'.$i.'" style="height:30px;">';

                            $val .= '<td style="background-color:'.$roomcolor.';width:'.$fwidth.'%">'.$room->name.'</td>';
                            $extra = 0;  // booked date increase value

                            $reservation = DB::table('reservations')->where('room_id', $room->id)->first();
                            if(!empty($reservation)){
                                $jsreservations[] = '"'.$reservation->name.'"';
                                $jsreservationids[] = '"'.$reservation->id.'"';
                            }else{
                                $jsreservations[] = '""';
                                $jsreservationids[] = '"0"';
                            }
                            for($j = 1; $j < $days; $j++){
                                if(!empty($reservation)){
                                    $paid = 'Pending';
                                    if($reservation->paid == 1){
                                        $paid = 'Paid';
                                    }

                                    $k = $j -1;
                                    $d1= date('Y/m/d', strtotime("+".$k." days"));
                                    $date_aux = date_create_from_format('Y/m/d', $d1);
                                    $date =  date_format($date_aux,'Y-m-d');
                                    $daytime = strtotime($date);

                                    $date_aux = date_create_from_format('Y/m/d', $reservation->start);
                                    $date =  date_format($date_aux,'Y-m-d');

                                    $start1 = strtotime($date);
                                    $date_aux = date_create_from_format('Y/m/d', $reservation->end);
                                    $date =  date_format($date_aux,'Y-m-d');
                                    $end1 = strtotime($date);
                                    $datediff1 = $end1 - $start1;
                                    $diffday2 = floor($datediff1/(60*60*24)) + 2;
                                    if($d1 == $reservation->start){
                                        $jsApAs[$i] = '"k"';
                                        $extra = $j;
                                        $cols = 2 * ($diffday2 - 1);
                                        $val .= '<td class="pren" colspan="'.$cols.'" style="width:'.$width.'%">';
                                        $val .= '<table id="prn'.$i.'" style="background-color: #FF9900;">';
                                        $val .= '<tr><td></td><td><a href="#" onclick="changeReservation(\''.$room->id.'\',\''.$room->name.'\',\''.$reservation->start.'\',\''.$reservation->end.'\',\''.$reservation->name.'\',\''.$reservation->id.'\')" style="color:#151617;">'.$reservation->name.' <span style="font-size:13px;color:#ebefee;">('.$paid.')</span></a></td><td></td>';
                                        $val .= '</tr>';
                                        $val .= '</table>';
                                        $val .= '</td>';
                                    }else if($daytime >= $start1 && $daytime <= $end1){
                                    }else{
                                        $val .= '<td colspan="2" style="width:'.$width.'%">&nbsp;</td>';
                                    }
                                }else{
                                    $val .= '<td colspan="2" style="width:'.$width.'%">&nbsp;</td>';
                                }
                            }
                            $val .= '</tr>';
                        }
                        echo $val.'</tbody></table>';
                        ?>
                    </div>
                </div>

                <form id="mod_pren" accept-charset="utf-8" method="post" action="javascript:alert('drag and drop action part.');">
                    <input type="hidden" name="anno" value="2016">
                    <input type="hidden" name="id_sessione" value="">
                    <input type="hidden" id="mese_orig" name="mese" value="7">
                    <input type="hidden" name="modificaprenotazione" value="1">
                    <input type="hidden" id="orig" name="origine" value="tab_mese_drop##">
                    <input type="hidden" id="id_pren" name="id_prenota" value="">
                    <input type="hidden" id="n_appart" name="n_appartamento" value="">
                    <input type="hidden" id="s_appart" name="sposta_appartamento" value="">
                    <input type="hidden" id="d_data_ins" name="d_data_inserimento" value="">
                    <input type="hidden" id="n_ini_per" name="n_inizioperiodo" value="">
                    <input type="hidden" id="n_fin_per" name="n_fineperiodo" value="">
                </form>
                <script type="text/javascript">
                    <!--
                    var priv_mod_aa = 's';
                    var priv_mod_da = 's';

                    var arr_app = <?php echo '[' . implode(',', $jsrooms) . ']';?>;
                    var arrroomno = <?php echo '[' . implode(',', $jsroomnos) . ']';?>;
                    var reservations = <?php echo '[' . implode(',', $jsreservations) . ']';?>;
                    var reservation_ids = <?php echo '[' . implode(',', $jsreservationids) . ']';?>;
                    var ApAs = <?php echo '[' . implode(',', $jsApAs) . ']';?>;
                    var DaIn = new Array();

                    var today = new Date();
                    var dd = today.getDate();
                    var mm = today.getMonth()+1; //January is 0!
                    var hh = today.getHours();
                    var min = today.getMinutes();
                    var second = today.getSeconds();

                    var yyyy = today.getFullYear();
                    if(dd<10){
                        dd='0'+dd
                    }
                    if(mm<10){
                        mm='0'+mm
                    }
                    if(hh<10){
                        hh='0'+hh
                    }
                    if(min<10){
                        min='0'+min
                    }
                    if(second<10){
                        second='0'+second
                    }
                    var today1 = yyyy+'-'+mm+'-'+dd+' '+hh+':'+min+':'+second;
                    DaIn[1] = today1;

                    var ArDaCo = <?php echo '[' . implode(',', $jsArDaCo) . ']';?>;

                    var ArTiOr = new Array();
                    var ArTiPr = new Array();

                    attiva_drag_drop();
                    attiva_colora_date(allinea_tab_mesi);
                    -->
                </script>

                <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            {!! Form::open(['url'=>url('/admin/change-reservation'),'class'=>'review-from', 'id'=>'answerform','enctype'=>'multipart/form-data']) !!}
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h3 class="modal-title" style="color:#2691d9" id="addModalLabel">Add Reservation</h3>
                            </div>
                            <div class="modal-body">
                                <br>

                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="client">Reservation Name : </label>
                                        <div class="col-sm-5">
                                            <input type="hidden" class="form-control" required id="reservationid" name="reservationid" value="0">
                                            <input type="text" class="form-control" required id="reservationname" name="reservationname" placeholder="Reservation name" value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="client">Selected Room : </label>
                                        <div class="col-sm-5">
                                            <input type="hidden" class="form-control" required id="roomname" name="roomname" placeholder="Room name" value="">
                                            <?php
                                            $rooms = DB::table('rooms')->orderby('id', 'asc')->get();
                                            ?>
                                            <select id="roomid" name="roomid" class="form-control">
                                                <?php
                                                foreach($rooms as $room){
                                                    echo '<option value="'.$room->id.'">'.$room->name.'</option>';
                                                }
                                                ?>
                                            </select>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="client">Start Date : </label>
                                        <div class="col-sm-5">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control" id="startdate" name="startdate" style="width:200px;"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-4" for="client">End Date : </label>
                                        <div class="col-sm-5">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                                <input type="text" class="form-control" id="enddate" name="enddate" style="width:200px;"/>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="modal-footer " id="createButton">
                                    <span type="button" class="btn btn-success btn-sm" data-dismiss="modal" onClick="onAddReservation()" style="background-color: #3C8DBC;border-color: #3C8DBC;">
                                        <span class="glyphicon glyphicon-ok"></span>
                                        <b> Save</b>
                                    </span>
                                    <span type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
                                        <span class="glyphicon glyphicon-remove"></span>
                                        <b> Cancel</b>
                                    </span>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <div class="main">


    </div>
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')

<!-- begining of page level js -->
<script src="/public/assets/vendors/moment/js/moment.min.js" type="text/javascript"></script>
<script src="/public/assets/vendors/daterangepicker/js/daterangepicker.js" type="text/javascript"></script>
<script src="/public/assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

<script src="/public/assets/vendors/clockface/js/clockface.js" type="text/javascript"></script>
<script src="/public/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js" type="text/javascript"></script>
<script src="/public/assets/js/pages/datepicker.js" type="text/javascript"></script>
<script>
    function onAddReservation(){
        $("#answerform").submit();
    }
    function changeReservation(roomid, roomname, start, end, name, reservationid){
        $('#startdate').val(start);
        $('#enddate').val(end);
        $('#roomid').val(roomid);
        //$('#roomname').val(roomname);
        $('#reservationname').val(name);
        $('#reservationid').val(reservationid);
        $('#addModalLabel').html('Change Reservation');
        $('#addModal').modal('show');
    }
</script>
@stop