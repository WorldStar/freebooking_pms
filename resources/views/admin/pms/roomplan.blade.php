@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Room Plans
    @parent
    @stop

    {{-- page level styles --}}
    @section('header_styles')

            <!--page level styles ends-->
    <link href="/assets/vendors/daterangepicker/css/daterangepicker.css" rel="stylesheet" type="text/css" />
    <link href="/assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/vendors/clockface/css/clockface.css" rel="stylesheet" type="text/css"/>
    <link href="/assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/assets/css/pages/buttons.css" />
@stop
{{-- Page content --}}
@section('content')

    <section class="content-header" style="margin-bottom: 0px !important;">
        <div style="float: left;margin-right: 10px;"><h1 style="vertical-align: top;margin-top: -1px;">Room Plan - </h1></div>
        <div class="input-group">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            <?php
            $startdate2 = date_create_from_format('Y-m-d', $startdate1);
            $startdate2 =  date_format($startdate2,'Y/m/d');
            $enddate2 = date_create_from_format('Y-m-d', $enddate1);
            $enddate2 =  date_format($enddate2,'Y/m/d');
            $defaultdate1 = date_create_from_format('Y-m-d', $defaultdate);
            $defaultdate1 =  date_format($defaultdate1,'Y/m/d');
            ?>
            <input type="text" class="form-control" id="defaultdaterange" style="width:300px;" value="{!! $startdate2 !!} - {!! $enddate2 !!}"/>
        </div>
    </section>
    <section class="content-header" style="margin-bottom: 0px !important;min-height: 74px;">


            <div style="width:500px;">
                <div style="margin-right: 10px;">
                    <span style="font-size: 15px;">@if(!empty($msg_type)){!! $msg !!}@endif</span>
                </div>
            </div>


        <div style="float:right;margin-right: 150px;">
            <div class="tagright" style="float: right;">
                <div style="margin-right: 0px;font-size: 15px;text-align: right;">Right Tag</div>
                <div class="input-group">
                    <button type="button" class="btn btn-responsive button-alignment btn-success" onclick="setMoveDate(1)" style="margin-bottom:7px;" data-toggle="+1">+1 ></button>&nbsp;
                    <button type="button" class="btn btn-responsive button-alignment btn-success" onclick="setMoveDate(7)" style="margin-bottom:7px;" data-toggle="+7">+7 >></button>&nbsp;
                    <button type="button" class="btn btn-responsive button-alignment btn-success" onclick="setMoveDate(21)" style="margin-bottom:7px;" data-toggle="+21">+21 >></button>
                </div>
            </div>
            <div class="tagcenter" style="float: right;">
                <div style="margin-right: 10px;"><span style="font-size: 15px;">&nbsp;</span></div>
                <div class="input-group">
                    <button type="button" class="btn btn-responsive button-alignment btn-warning" onclick="setMoveDate(0)" style="margin-bottom:7px;" data-toggle="Day">Day</button>&nbsp;
                </div>
            </div>
            <div class="tagleft" style="float: right;">
                <div style="margin-right: 10px;"><span style="font-size: 15px;">Left Tag</span></div>
                <div class="input-group">
                    <button type="button" class="btn btn-responsive button-alignment btn-primary" onclick="setMoveDate(-21)" style="margin-bottom:7px;" data-toggle="-21"><< -21</button>&nbsp;
                    <button type="button" class="btn btn-responsive button-alignment btn-primary" onclick="setMoveDate(-7)" style="margin-bottom:7px;" data-toggle="-7"><< -7</button>&nbsp;
                    <button type="button" class="btn btn-responsive button-alignment btn-primary" onclick="setMoveDate(-1)" style="margin-bottom:7px;" data-toggle="-1">< -1</button>&nbsp;
                </div>
            </div>

        </div>
    </section>
    <!-- Main content -->
    <section class="content">
        <div id="header">
            <div class="bg-help">
                <div class="inBox">
                    <hr class="hidden" />
                    <link rel="stylesheet" type="text/css" href="/js/druid/base.css" media="all">
                    <link rel="stylesheet" type="text/css" href="/js/druid/inc/stylesheet.css" media="all">
                    <link rel="stylesheet" type="text/css" href="/js/druid/inc/screen.css" media="screen">
                    <link rel="stylesheet" type="text/css" href="/js/druid/inc/paper.css" media="print">
                    <script type="text/javascript" src="/js/druid/inc/functions.js">
                    </script>
                    <script type="text/javascript" src="/js/druid/base.js"></script>
                    <span id="m_corr_su"></span>

                    <div style="position: relative; margin-left: 2%; margin-top: 10px">
                        <?php


                        $date_aux = date_create_from_format('Y-m-d', $startdate1);
                        $date =  date_format($date_aux,'Y-m-d');
                        $now = strtotime($date);

                        $date_aux = date_create_from_format('Y-m-d', $enddate1);
                        $date =  date_format($date_aux,'Y-m-d');
                        $your_date = strtotime($date);

                        $datediff = $your_date - $now;

                        $days = floor($datediff/(60*60*24)) + 2;

                        $width = 93 / $days;
                        $fwidth = 100 - $days *$width;
                        $val = '<table style="width:96%;" cellpadding="0" border=0><tbody>';
                        $val .= '<tr>';
                        $val .= '<td style="padding: 0 2px 0 2px;width:'.$fwidth.'%"></td>';
                        $flg = 0;
                        for($i = 1; $i < $days-1; $i++){
                            $j = $i-1;
                            $NewDate= date('M, Y', strtotime("+".$j." days"));
                            $dd= date('d', strtotime("+".$j." days"));
                            if($i == 1 && $dd != 1){
                                $flg = 1;
                                $val .= '<td colspan="4" style="width:'.($width * 2).'%">'.$NewDate.'</td>';
                            }else if($dd == 1){
                                $flg = 2;
                                $val .= '<td colspan="4" style="width:'.($width * 2).'%">'.$NewDate.'</td>';
                            }else{
                                if($flg == 1 || $flg == 2){
                                    $flg = 0;
                                }else{
                                    $val .= '<td colspan="2" style="width:'.$width.'%">&nbsp;</td>';
                                }
                            }
                        }
                        $val .= '</tr></table>';
                        echo $val;
                        ?>
                    <div>
                    <div style="position: relative;"  ondragover="event.preventDefault();" ondragenter="event.preventDefault();" ondrop="event.preventDefault();drp_out(event);">
                        <span id="m_corr_su"></span>
                        <script type="text/javascript">
                            <!--
                            var colore_date_norm = '#daedff';
                            var colore_date_altre = '#b7dcff';
                            var colore_date_sel = '#ffffff';
                            var colore_drp1 = '#05e105';
                            var colore_drp2 = '#297929';

                            var id_ini_tab = 182;
                            var id_fine_tab = 213;
                            var agg_tronca = -2;
                            var riduci_font = 0;
                            var tipo_periodi = 'g';
                            var id_sessione = '';
                            var d = new Date();
                            var anno = d.getFullYear();
                            var allinea_tab_mesi = 'NO';
                            var tab_spostata = 0;
                            var sel_start_date = 0;
                            var sel_start_col = 0;
                            var sel_stop_date = 0;
                            var curr_sel_row = 0;
                            -->
                        </script>

                        <div style="position: relative;" ondragstart="drg(event)">
                            <?php
                            //$currentdate = date('d-m-Y', time());
                            //$enddate = date('d-m-Y', strtotime('+1 months'));


                            $date_aux = date_create_from_format('Y-m-d', $defaultdate);
                            $date =  date_format($date_aux,'Y-m-d');
                            $default_date = strtotime($date);
                            $date_aux = date_create_from_format('Y-m-d', $startdate1);
                            $date =  date_format($date_aux,'Y-m-d');
                            $start_date = strtotime($date);
                            $date_aux = date_create_from_format('Y-m-d', $enddate1);
                            $date =  date_format($date_aux,'Y-m-d');
                            $your_date = strtotime($date);

                            $datediff = $your_date - $start_date;

                            $days = floor($datediff/(60*60*24)) + 2;


                            $rooms = DB::table('rooms')->orderby('id', 'asc')->get();


                            $topbars = array();
                            $dayarray = array();
                            $week = array('Su', 'Mon', 'Tu', 'We', 'Th', 'Fr', 'Sa');
                            for($i = 1; $i < $days; $i++){
                                $j = $i-1;
                                $NewDate= date('y-m-d', strtotime($startdate1. "+".$j." days"));
                                $d1= date('d', strtotime($startdate1. "+".$j." days"));
                                $m1= date('w', strtotime($startdate1. "+".$j." days"));
                                $topbars[$i] = array($d1, $week[$m1]);
                                $dayarray[$i] = $NewDate;

                            }
                            $width = 93 / $days;
                            $fwidth = 100 - $days *$width;
                            $val = '<table id="roomplantable" class="m1" style="background-color: #b9ccd4;width:96%;" cellpadding="3" ><tbody>';
                            $val .= '<tr class="rd_r" style="background-color: #daedff;">';
                            $val .= '<td style="background-color: #b9ccd4; padding: 0 2px 0 2px;width:'.$fwidth.'%"><a name="rd_n1"></a>Rooms</td>';

                            $jsArDaCo = array();
                            for($i = 1; $i < $days; $i++){
                                if($topbars[$i][1] == 'Su'){
                                    $val .= '<td class="rd_'.$i.'" colspan="2"><b style="color: red;">'.$topbars[$i][0].'<small><br>'.$topbars[$i][1].'</small></b></td>';
                                }else{
                                    $val .= '<td class="rd_'.$i.'" colspan="2">'.$topbars[$i][0].'<small><br>'.$topbars[$i][1].'</small></td>';
                                }
                                $k = $i -1;
                                $d1= date('Y-m-d', strtotime($startdate1. "+".$k." days"));
                                $jsArDaCo[] = '"'.$d1.'"';
                            }
                            $val .= '</tr>';

                            $extra = 0;
                            $i = 0;
                            $jsrooms = array();
                            $jsroomnos = array();
                            $jsreservations = array();
                            $jsreservationids = array();
                            $jsApAs = array();
                            foreach($rooms as $room){
                                $jsrooms[] = '"'.$room->name.'"';
                                $jsroomnos[] = '"'.$room->id.'"';
                                $jsApAs[] = '""';
                            }
                            $jsroomcolor = array();
                            $jsroomcnt = 0;
                            $ii = 0;
                            foreach($rooms as $room){
                                $i++;
                                $roomcolor = DB::table('room_types')->where('id', $room->type_id)->first()->color;
                                $jsroomcolor[] = '"'.$roomcolor.'"';
                                $jsroomcnt++;
                                $val .= '<tr id="app'.$i.'" class="trapp" style="height:30px;">';

                                $val .= '<td class="trapp'.$i.'" style="background-color:'.$roomcolor.';width:'.$fwidth.'%">'.$room->name.'</td>';
                                $extra = 0;  // booked date increase value


                                for($j = 1; $j < $days; $j++){

                                    $k = $j -1;
                                    $d1= date('Y-m-d', strtotime($startdate1. "+".$k." days"));
                                    $reservation = DB::table('reservations')->where('room_id', $room->id)->where('start', '<=', $d1)->where('end', '>=', $d1)->first();
                                    if(!empty($reservation)){
                                        $date_aux = date_create_from_format('Y-m-d', $reservation->start);
                                        $date =  date_format($date_aux,'Y-m-d');
                                        $start11 = strtotime($date);
                                        $date_aux = date_create_from_format('Y-m-d', $startdate1);
                                        $date =  date_format($date_aux,'Y-m-d');
                                        $startdate11 = strtotime($date);
                                        $startdate22 = $reservation->start;
                                        if($startdate11 >$start11){
                                            $startdate22 = $startdate1;
                                        }
                                        $date_aux = date_create_from_format('Y-m-d', $reservation->end);
                                        $date =  date_format($date_aux,'Y-m-d');
                                        $end11 = strtotime($date);
                                        $date_aux = date_create_from_format('Y-m-d', $enddate1);
                                        $date =  date_format($date_aux,'Y-m-d');
                                        $enddate11 = strtotime($date);
                                        $enddate22 = $reservation->end;
                                        if($enddate11 < $end11){
                                            $enddate22 = $enddate1;
                                        }

                                        $paid = 'Pending';
                                        if($reservation->paid == 1){
                                            $paid = 'Paid';
                                        }
                                        $date_aux = date_create_from_format('Y-m-d', $d1);
                                        $date =  date_format($date_aux,'Y-m-d');
                                        $daytime = strtotime($date);

                                        $date_aux = date_create_from_format('Y-m-d', $startdate22);
                                        $date =  date_format($date_aux,'Y-m-d');

                                        $start1 = strtotime($date);

                                        $date_aux = date_create_from_format('Y-m-d', $enddate22);
                                        $date =  date_format($date_aux,'Y-m-d');
                                        $end1 = strtotime($date);

//                                        $date_aux = date_create_from_format('Y-m-d', $enddate1);
//                                        $date =  date_format($date_aux,'Y-m-d');
//
//                                        $enddate3 = strtotime($date);
                                        $datediff1 = $end1 - $start1;
//                                        if($enddate3 < $end1){
//                                            $datediff1 = $enddate3 - $start1;
//                                        }
                                        $diffday2 = floor($datediff1/(60*60*24)) + 2;

                                        if($d1 == $startdate22){
                                            $ii++;
                                             $jsreservations[$ii] = '"'.$reservation->name.'"';
                                             $jsreservationids[$ii] = '"'.$reservation->id.'"';
                                            $jsApAs[$ii] = '"k"';
                                            $extra = $j;
                                            $cols = 2 * ($diffday2 - 1);
                                            $val .= '<td class="pren" colspan="'.$cols.'" style="width:'.($width * ($diffday2 - 1)).'%">';
                                            $val .= '<table id="prn'.$ii.'" style="background-color: '.$roomcolor.';">';
                                            $val .= '<tr><td></td><td><a href="#" onclick="changeReservation(\''.$room->id.'\',\''.$room->name.'\',\''.$reservation->start.'\',\''.$reservation->end.'\',\''.$reservation->name.'\',\''.$reservation->id.'\')" style="color:#151617;white-space: nowrap;text-overflow: ellipsis;	overflow: hidden;width:'.($width * ($diffday2 - 1)).'%">'.$reservation->name.' <span style="font-size:13px;color:#ebefee;">('.$paid.')</span></a></td><td></td>';
                                            $val .= '</tr>';
                                            $val .= '</table>';
                                            $val .= '</td>';
                                        }else if($daytime >= $start1 && $daytime <= $end1){

                                        }else{
                                            $val .= '<td id="td'.$j.'" colspan="2" style="width:'.$width.'%">&nbsp;</td>';
                                        }
                                    }else{
                                        $val .= '<td id="td'.$j.'" colspan="2" style="width:'.$width.'%">&nbsp;</td>';
                                    }
                                }

                                $val .= '</tr>';
                            }
                            echo $val.'</tbody></table>';
                            ?>
                        </div>

                    </div>

                    <form id="mod_pren" accept-charset="utf-8" method="post" action="javascript:alert('drag and drop action part.');">
                        <input type="hidden" name="anno" value="2016">
                        <input type="hidden" name="id_sessione" value="">
                        <input type="hidden" id="mese_orig" name="mese" value="7">
                        <input type="hidden" name="modificaprenotazione" value="1">
                        <input type="hidden" id="orig" name="origine" value="tab_mese_drop##">
                        <input type="hidden" id="id_pren" name="id_prenota" value="">
                        <input type="hidden" id="n_appart" name="n_appartamento" value="">
                        <input type="hidden" id="s_appart" name="sposta_appartamento" value="">
                        <input type="hidden" id="d_data_ins" name="d_data_inserimento" value="">
                        <input type="hidden" id="n_ini_per" name="n_inizioperiodo" value="">
                        <input type="hidden" id="n_fin_per" name="n_fineperiodo" value="">


                    </form>



                    <script type="text/javascript">
                        <!--
                        var priv_mod_aa = 's';
                        var priv_mod_da = 's';
                        var roomcolor = <?php echo '[' . implode(',', $jsroomcolor) . ']';?>;
                        var arr_app = <?php echo '[' . implode(',', $jsrooms) . ']';?>;

                        var arrroomno = <?php echo '[' . implode(',', $jsroomnos) . ']';?>;
                        var reservations = <?php echo '[' . implode(',', $jsreservations) . ']';?>;
                        var reservation_ids = <?php echo '[' . implode(',', $jsreservationids) . ']';?>;

                        var ApAs = <?php echo '[' . implode(',', $jsApAs) . ']';?>;

                        var DaIn = new Array();

                        var today = new Date('{!! $startdate1 !!}');
                        var dd = today.getDate();
                        var mm = today.getMonth()+1; //January is 0!
                        var hh = today.getHours();
                        var min = today.getMinutes();
                        var second = today.getSeconds();

                        var yyyy = today.getFullYear();
                        if(dd<10){
                            dd='0'+dd
                        }
                        if(mm<10){
                            mm='0'+mm
                        }
                        if(hh<10){
                            hh='0'+hh
                        }
                        if(min<10){
                            min='0'+min
                        }
                        if(second<10){
                            second='0'+second
                        }
                        var today1 = yyyy+'-'+mm+'-'+dd+' '+hh+':'+min+':'+second;
                        DaIn[1] = today1;

                        var ArDaCo = <?php echo '[' . implode(',', $jsArDaCo) . ']';?>;

                        var ArTiOr = new Array();
                        var ArTiPr = new Array();

                        attiva_drag_drop();
                        attiva_colora_date(allinea_tab_mesi);
                        -->
                    </script>

                    <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document" style="width:80%;height:80%;font-size:15px;">
                            <div class="modal-content" style="width:100%;height:100%;font-size:15px;">
                                {!! Form::open(['url'=>url('/admin/change-reservation'),'class'=>'review-from', 'id'=>'answerform','enctype'=>'multipart/form-data']) !!}
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h3 class="modal-title" style="color:#2691d9" id="addModalLabel">Add Reservation</h3>
                                </div>
                                <div class="modal-body">
                                    <br>

                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="client">Reservation Name : </label>
                                            <div class="col-sm-5">
                                                <input type="hidden" class="form-control" required id="reservationid" name="reservationid" value="0">
                                                <input type="text" class="form-control" required id="reservationname" name="reservationname" placeholder="Reservation name" value="">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="client">Selected Room : </label>
                                            <div class="col-sm-5">
                                                <input type="hidden" class="form-control" required id="roomname" name="roomname" placeholder="Room name" value="">
                                                <?php
                                                $rooms = DB::table('rooms')->orderby('id', 'asc')->get();
                                                ?>
                                                <select id="roomid" name="roomid" class="form-control">
                                                    <?php
                                                    foreach($rooms as $room){
                                                        echo '<option value="'.$room->id.'">'.$room->name.'</option>';
                                                    }
                                                    ?>
                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="client">Start Date : </label>
                                            <div class="col-sm-5">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control" id="startdate" name="startdate" style="width:200px;"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-4" for="client">End Date : </label>
                                            <div class="col-sm-5">
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                        <i class="fa fa-calendar"></i>
                                                    </div>
                                                    <input type="text" class="form-control" id="enddate" name="enddate" style="width:200px;"/>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="modal-footer " id="createButton" style="position: absolute;bottom: 0px;width: 100%;">
                                    <span type="button" class="btn btn-success btn-sm" data-dismiss="modal" onClick="onAddReservation()" style="background-color: #3C8DBC;border-color: #3C8DBC;">
                                        <span class="glyphicon glyphicon-ok"></span>
                                        <b> Save</b>
                                    </span>
                                    <span type="button" class="btn btn-danger btn-sm" data-dismiss="modal" onclick="onClickCancel()">
                                        <span class="glyphicon glyphicon-remove"></span>
                                        <b> Cancel</b>
                                    </span>
                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <div class="main">


        </div>
    </section>
    @stop

    {{-- page level scripts --}}
    @section('footer_scripts')

            <!-- begining of page level js -->
    <script src="/assets/vendors/moment/js/moment.min.js" type="text/javascript"></script>
    <script src="/assets/vendors/daterangepicker/js/daterangepicker.js" type="text/javascript"></script>
    <script src="/assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

    <script src="/assets/vendors/clockface/js/clockface.js" type="text/javascript"></script>
    <script src="/assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js" type="text/javascript"></script>
    <script src="/assets/js/pages/datepicker.js" type="text/javascript"></script>
    <script>
        var startdate1 = '{!! $startdate1 !!}';
        var enddate1 = '{!! $enddate1 !!}';
        var defaultdate = '{!! $defaultdate !!}';
        var moveval = '{!! $moveval !!}';
        var roomcnt = '{!! $jsroomcnt !!}'
        var rangeflg = false;
        var defaultflg = false;
        var trid = '';
        var tdfirstcolor = '';
        var tdlastcolor = '';
        var isMouseDown = false;

        function onClickCancel(){
            if(trid != '') {
                //var cas1 = document.getElementById(trid).firstChild;
                //var cas2 = document.getElementById(trid).lastChild;
                //cas1.style.backgroundColor = tdfirstcolor;
                for(var i = 1; i <= roomcnt; i++) {
                    $('.trapp'+i).css('background-color', roomcolor[i-1]);
                }
                //cas2.style.backgroundColor = tdlastcolor;
                trid = '';
                tdfirstcolor = '';
            }
        }
        function onAddReservation(){
            $("#answerform").submit();
        }
        function changeReservation(roomid, roomname, start, end, name, reservationid){
            $('#startdate').val(start);
            $("#enddate").daterangepicker({
                singleDatePicker: true,
                minDate:start,
                showDropdowns: true,
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });
            $('#enddate').val(end);
            $('#roomid').val(roomid);
            //$('#roomname').val(roomname);
            $('#reservationname').val(name);
            $('#reservationid').val(reservationid);
            $('#addModalLabel').html('Change Reservation');
            $('#addModal').modal('show');
        }
        function setEndDate(startdate){
            $("#enddate").daterangepicker({
                singleDatePicker: true,
                minDate:startdate,
                showDropdowns: true,
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });
        }
        var daterange = $("#defaultdaterange").daterangepicker({
            locale: {
                format: 'YYYY/MM/DD'
            }
        },setRangeData);

//        $('#startdate').datepicker({
//            minDate: startdate1,
//            autoclose: true
//        });
        var startpicker = $("#startdate").daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'YYYY-MM-DD'
            }
        }, setData);
        var endpicker = $("#enddate").daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: 'YYYY-MM-DD'
            }
        });

        function setRangeData(start, end) {
            if(rangeflg) {
                $('#defaultdaterange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                console.log("default1:" + start.format('YYYY-MM-DD'));
                console.log("default2:" + end.format('YYYY-MM-DD'));
                startdate1 = start.format('YYYY-MM-DD');
                enddate1 = end.format('YYYY-MM-DD');
                window.location = '/admin/reservation/'+startdate1+'/'+enddate1+'/'+moveval;
            }
            console.log("default1");
            rangeflg = true;

        }
        setRangeData(moment().subtract(29, 'days'), moment());
        setData(moment());
        function setMoveDate(movedate){
            moveval = movedate;
            window.location = '/admin/reservation/'+startdate1+'/'+enddate1+'/'+moveval;
        }
        function setData(start){
            console.log(start.format('MMMM D, YYYY'));
            var min = '"'+start.format('YYYY-MM-DD')+'"';
            $("#enddate").daterangepicker({
                singleDatePicker: true,
                minDate:min,
                showDropdowns: true,
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });

        }
//        $(function (){
//           var index = -1;
//            $('#roomplantable tr').mouseover(function (e){
//                //console.log('ismousedown:'+isMouseDown);
//                if(isMouseDown) {
//                    var index1 = $(this).index();
//                    console.log("a:" + index1);
//                }
//            });
//            $('#roomplantable tr').mousedown(function (e){
//
//                isMouseDown = true;
//                var index1 = $(this).index();
//
//            });
//        });
    </script>
@stop