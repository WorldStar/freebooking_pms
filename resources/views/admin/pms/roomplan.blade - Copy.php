@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
Room Plans
@parent
@stop

{{-- page level styles --}}
@section('header_styles')


    <!--page level styles ends-->
    <link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/clockface/css/clockface.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('assets/css/pages/buttons.css') }}" />
@stop

{{-- Page content --}}
@section('content')

                <section class="content-header" style="margin-bottom: 0px !important;">
                    <div style="float: left;margin-right: 10px;"><h1 style="vertical-align: top;margin-top: -1px;">Room Plan - </h1></div>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control" id="daterange1" style="width:300px;"/>

                    </div>
                </section>
                <section class="content-header" style="margin-bottom: 0px !important;min-height: 74px;">
                    <div style="float:left;width:220px;">
                        <div style="margin-right: 10px;"><span style="font-size: 15px;">Select Day</span></div>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control" id="rangepicker4" style="width:200px;"/>
                        </div>
                    </div>
                    <div style="float:right;">
                        <div class="tagright" style="float: right;">
                            <div style="margin-right: 0px;font-size: 15px;text-align: right;">Right Tag</div>
                            <div class="input-group">
                                <button type="button" class="btn btn-responsive button-alignment btn-success" style="margin-bottom:7px;" data-toggle="button">+1 ></button>&nbsp;
                                <button type="button" class="btn btn-responsive button-alignment btn-success" style="margin-bottom:7px;" data-toggle="button">+7 >></button>&nbsp;
                                <button type="button" class="btn btn-responsive button-alignment btn-success" style="margin-bottom:7px;" data-toggle="button">+21 >></button>
                            </div>
                        </div>
                        <div class="tagcenter" style="float: right;">
                            <div style="margin-right: 10px;"><span style="font-size: 15px;">&nbsp;</span></div>
                            <div class="input-group">
                                <button type="button" class="btn btn-responsive button-alignment btn-warning" style="margin-bottom:7px;" data-toggle="button">Day</button>&nbsp;
                            </div>
                        </div>
                        <div class="tagleft" style="float: right;">
                            <div style="margin-right: 10px;"><span style="font-size: 15px;">Left Tag</span></div>
                            <div class="input-group">
                                <button type="button" class="btn btn-responsive button-alignment btn-primary" style="margin-bottom:7px;" data-toggle="button">< -1</button>&nbsp;
                                <button type="button" class="btn btn-responsive button-alignment btn-primary" style="margin-bottom:7px;" data-toggle="button"><< -7</button>&nbsp;
                                <button type="button" class="btn btn-responsive button-alignment btn-primary" style="margin-bottom:7px;" data-toggle="button"><< -21</button>&nbsp;
                            </div>
                        </div>

                    </div>
                </section>
                <!-- Main content -->

                <section class="content">
                    <div id="header">
                        <div class="bg-help">
                            <div class="inBox">
                                <hr class="hidden" />
                                <link rel="stylesheet" type="text/css" href="../js/druid/base.css" media="all">
                                <link rel="stylesheet" type="text/css" href="../js/druid/inc/stylesheet.css" media="all">
                                <link rel="stylesheet" type="text/css" href="../js/druid/inc/screen.css" media="screen">
                                <link rel="stylesheet" type="text/css" href="../js/druid/inc/paper.css" media="print">
                                <script type="text/javascript" src="../js/druid/inc/functions.js">
                                </script>
                                <script type="text/javascript" src="../js/druid/base.js"></script>
                                <span id="m_corr_su"></span>

                                <div style="position: relative;"  ondragover="event.preventDefault();" ondragenter="event.preventDefault();" ondrop="event.preventDefault();drp_out();">
                                    <span id="m_corr_su"></span>
                                    <script type="text/javascript">
                                        <!--
                                        var colore_date_norm = '#daedff';
                                        var colore_date_altre = '#b7dcff';
                                        var colore_date_sel = '#ffffff';
                                        var colore_drp1 = '#05e105';
                                        var colore_drp2 = '#297929';

                                        var id_ini_tab = 182;
                                        var id_fine_tab = 213;
                                        var agg_tronca = -2;
                                        var riduci_font = 0;
                                        var tipo_periodi = 'g';
                                        var id_sessione = '';
                                        var anno = 2016;
                                        var allinea_tab_mesi = 'NO';
                                        var tab_spostata = 0;
                                        var sel_start_date = 0;
                                        var sel_start_col = 0;
                                        var sel_stop_date = 0;
                                        var curr_sel_row = 0;
                                        -->
                                    </script>

                                    <div style="position: relative; margin-left: 2%; margin-top: 10px" ondragstart="drg(event)">
                                        <?php
                                        $val = '<table class="m1" style="background-color: #b9ccd4;width:96%;" cellpadding="3" ><tbody>';
                                        $val .= '<tr class="rd_r" style="background-color: #daedff;">';
                                        $val .= '<td style="background-color: #b9ccd4; padding: 0 2px 0 2px"><a name="rd_n1"></a>Rooms</td>';
                                        for($i = 1; $i < 32; $i++){

                                            if($i % 7 == 3){
                                                $val .= '<td class="rd_'.$i.'" colspan="2"><b style="color: red;">'.$i.'<small><br>Su</small></b></td>';
                                            }else{
                                                $val .= '<td class="rd_'.$i.'" colspan="2">'.$i.'<small><br>Fr</small></td>';
                                            }
                                        }
                                        $val .= '</tr>';
                                        $rooms = array('room1', 'room2', 'room3', 'room4', 'room5', 'room6', 'room7');
                                        $book3 = array('10', '5');
                                        $book4 = array('14', '6');
                                        $extra = 0;
                                        for($i = 0; $i < count($rooms); $i++){
                                            $val .= '<tr id="app'.$i.'" style="height:30px;">';
                                            $roomcolor = '#00bc8c';
                                            if($i > 3){
                                                $roomcolor = '#1c94c4';
                                            }
                                            $val .= '<td style="background-color:'.$roomcolor.'">'.$rooms[$i].'</td>';
                                            $extra = 0;  // booked date increase value
                                            for($j = 1; $j < 32; $j++){
                                                $check = $book3[0] + $extra;
                                                $max1 = $book3[0] + $book3[1];
                                                if($i == 4){
                                                    $check = $book4[0] + $extra;
                                                    $max1 = $book4[0] + $book4[1];
                                                }
                                                if(($i == 3 && $j < $max1) && $check == $j){
                                                    if($j == $book3[0]){
                                                        $cols = 2 * $book3[1];
                                                        $val .= '<td colspan="'.$cols.'">';
                                                        $val .= '<table id="prn1" style="background-color: #FF9900;width:100%">';
                                                        $val .= '<tr><td></td><td><a href="modifica_prenota.php?id_prenota=1&amp;anno=2016&amp;id_sessione=&amp;mese=7">Sin</a></td><td></td>';
                                                        $val .= '</tr>';
                                                        $val .= '</table>';
                                                        $val .= '</td>';
                                                    }
                                                    $extra ++;
                                                }
                                                else  if(($i == 4 && $j < $max1) && $check == $j){
                                                    if($j == $book4[0]){
                                                        $cols = 2 * $book4[1];
                                                        $val .= '<td colspan="'.$cols.'">';
                                                        $val .= '<table id="prn2" style="background-color: #FF9900;width:100%">';
                                                        $val .= '<tr><td></td><td><a href="modifica_prenota.php?id_prenota=1&amp;anno=2016&amp;id_sessione=&amp;mese=7">Sin</a></td><td></td>';
                                                        $val .= '</tr>';
                                                        $val .= '</table>';
                                                        $val .= '</td>';
                                                    }
                                                    $extra ++;
                                                }
                                                else
                                                {
                                                    $val .= '<td colspan="2">&nbsp;</td>';
                                                }
                                            }

                                            $val .= '</tr>';
                                        }
                                        echo $val.'</tbody></table>';
                                        ?>
                                    </div>
                                </div>





                                <form id="mod_pren" accept-charset="utf-8" method="post" action="javascript:alert('drag and drop action part.');">
                                    <input type="hidden" name="anno" value="2016">
                                    <input type="hidden" name="id_sessione" value="">
                                    <input type="hidden" id="mese_orig" name="mese" value="7">
                                    <input type="hidden" name="modificaprenotazione" value="2">
                                    <input type="hidden" id="orig" name="origine" value="tab_mese_drop##">
                                    <input type="hidden" id="id_pren" name="id_prenota" value="">
                                    <input type="hidden" id="n_appart" name="n_appartamento" value="">
                                    <input type="hidden" id="s_appart" name="sposta_appartamento" value="">
                                    <input type="hidden" id="d_data_ins" name="d_data_inserimento" value="">
                                    <input type="hidden" id="n_ini_per" name="n_inizioperiodo" value="">
                                    <input type="hidden" id="n_fin_per" name="n_fineperiodo" value="">
                                </form>

                                <script type="text/javascript">
                                    <!--
                                    var priv_mod_aa = 's';
                                    var priv_mod_da = 's';
                                    var arr_app = new Array("1","2a","2b","2c","2d","2e");
                                    var ApAs = new Array();
                                    ApAs[1] = 'k';

                                    var DaIn = new Array();
                                    DaIn[1] = '2016-07-13 16:10:59';

                                    var ArDaCo = new Array();
                                    ArDaCo[1] = '2016-06-30';
                                    ArDaCo[2] = '2016-07-01';
                                    ArDaCo[3] = '2016-07-02';
                                    ArDaCo[4] = '2016-07-03';
                                    ArDaCo[5] = '2016-07-04';
                                    ArDaCo[6] = '2016-07-05';
                                    ArDaCo[7] = '2016-07-06';
                                    ArDaCo[8] = '2016-07-07';
                                    ArDaCo[9] = '2016-07-08';
                                    ArDaCo[10] = '2016-07-09';
                                    ArDaCo[11] = '2016-07-10';
                                    ArDaCo[12] = '2016-07-11';
                                    ArDaCo[13] = '2016-07-12';
                                    ArDaCo[14] = '2016-07-13';
                                    ArDaCo[15] = '2016-07-14';
                                    ArDaCo[16] = '2016-07-15';
                                    ArDaCo[17] = '2016-07-16';
                                    ArDaCo[18] = '2016-07-17';
                                    ArDaCo[19] = '2016-07-18';
                                    ArDaCo[20] = '2016-07-19';
                                    ArDaCo[21] = '2016-07-20';
                                    ArDaCo[22] = '2016-07-21';
                                    ArDaCo[23] = '2016-07-22';
                                    ArDaCo[24] = '2016-07-23';
                                    ArDaCo[25] = '2016-07-24';
                                    ArDaCo[26] = '2016-07-25';
                                    ArDaCo[27] = '2016-07-26';
                                    ArDaCo[28] = '2016-07-27';
                                    ArDaCo[29] = '2016-07-28';
                                    ArDaCo[30] = '2016-07-29';
                                    ArDaCo[31] = '2016-07-30';
                                    ArDaCo[32] = '2016-07-31';
                                    ArDaCo[33] = '2016-08-01';

                                    var ArTiOr = new Array();
                                    var ArTiPr = new Array();

                                    attiva_drag_drop();
                                    attiva_colora_date(allinea_tab_mesi);
                                    -->
                                </script>

                                <form id="ins_pren" accept-charset="utf-8" method="post" action="javascript:alert('drag and drop action part.');">
                                    <input type="hidden" name="anno" value="2016">
                                    <input type="hidden" name="id_sessione" value="">
                                    <input type="hidden" name="num_tipologie" value="1">
                                    <input type="hidden" name="mos_tut_dat" value="SI">
                                    <input type="hidden" id="ins_ini_per" name="inizioperiodo1" value="">
                                    <input type="hidden" id="ins_fin_per" name="fineperiodo1" value="">
                                    <input type="hidden" id="ins_app" name="appartamento1" value="">
                                </form>

                            </div>
                        </div>
                    </div>
                    <div class="main">
                    </div>
                </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')

        <!-- begining of page level js -->
                <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
                <script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}" type="text/javascript"></script>
                <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>

                <script src="{{ asset('assets/vendors/clockface/js/clockface.js') }}" type="text/javascript"></script>
                <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" type="text/javascript"></script>
                <script src="{{ asset('assets/js/pages/datepicker.js') }}" type="text/javascript"></script>
@stop