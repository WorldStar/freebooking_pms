<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>
        @section('title')
            | Property Management System
        @show
    </title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>


    <![endif]-->
    <!-- global css -->
    <meta charset="utf-8">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Property Management System" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black"/>
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta name="robots" content="index, follow">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <script src="/public/js/html5shiv.js" type="text/javascript"></script>
    <script src="/public/js/respond.min.js" type="text/javascript"></script>
    <script src="/public/assets/js/jquery.min.js" type="text/javascript"></script>
    <script src="/public/assets/js/jquery-ui.min.js" type="text/javascript"></script>
    <script src="/public/js/daypilot/daypilot-all.min.js" type="text/javascript"></script>
    <link href="/public/assets/css/app.css" rel="stylesheet" type="text/css"/>
    <!-- font Awesome -->
    <script>
        /* $.ajaxSetup({
         headers: {
         'X-CSRF-TOKEN': "{{ csrf_token() }}"
         }
         }); */

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

    <!-- end of global css -->
    <!--page level css-->
    @yield('header_styles')
            <!--end of page level css-->

<body class="skin-josh">
<header class="header">
    <a href="{{ route('dashboard') }}" class="logo" style="background-color: #444954; !important; vertical-align: middle;text-align: left;padding-left: 20px;">
        <span style="color: #fff;font-size: 25px">PMS</span>
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <div>
            <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                <div class="responsive_nav"></div>
            </a>
        </div>


        <div class="navbar-right">
            <ul class="nav navbar-nav">
                @include('admin.layouts._messages')
                @include('admin.layouts._notifications')
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        @if(!empty(Sentinel::getUser()) && Sentinel::getUser()->pic)
                            <img src="{!! url('/').'/uploads/users/'.Sentinel::getUser()->pic !!}" alt="img" height="35px" width="35px"
                                 class="img-circle img-responsive pull-left"/>
                        @else
                            <img src="{!! asset('assets/img/authors/avatar3.jpg') !!} " width="35"
                                 class="img-circle img-responsive pull-left" height="35" alt="riot">
                        @endif
                        <div class="riot">
                            <div>
                                <?php if(!empty(Sentinel::getUser())){?>
                                <?php echo Sentinel::getUser()->first_name.' '.Sentinel::getUser()->last_name ;?>
                                <?php } else{ echo '';}?>
                                <span>
                                        <i class="caret"></i>
                                    </span>
                            </div>
                        </div>
                    </a>



                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header bg-light-blue">
                            @if(!empty(Sentinel::getUser()) && Sentinel::getUser()->pic)
                                <img src="{!! url('/').'/uploads/users/'.Sentinel::getUser()->pic !!}" alt="img"
                                     class="img-circle img-bor"/>
                            @else
                                <img src="{!! asset('assets/img/authors/avatar3.jpg') !!}"
                                     class="img-responsive img-circle" alt="User Image">
                            @endif
                            <p class="topprofiletext">
                                <?php if(!empty(Sentinel::getUser())){?>
                                <?php echo Sentinel::getUser()->first_name.' '.Sentinel::getUser()->last_name ;?>
                                <?php } else{ echo '';}?>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        <li>
                            <?php
                            if(!empty(Sentinel::getUser())){
                            ?>
                            <a href="{{ URL::route('users.show',Sentinel::getUser()->id) }}">
                                <?php
                                }
                                ?>

                                <i class="livicon" data-name="user" data-s="18"></i>
                                My Profile

                                <?php
                                if(!empty(Sentinel::getUser())){
                                ?>
                            </a>
                            <?php
                            }
                            ?>
                        </li>
                        <li role="presentation"></li>
                        <li>
                            <?php
                            if(!empty(Sentinel::getUser())){
                            ?>
                            <a href="{{ route('admin.users.edit', Sentinel::getUser()->id) }}">
                                <?php
                                }
                                ?>

                                <i class="livicon" data-name="gears" data-s="18"></i>
                                Account Settings
                                <?php
                                if(!empty(Sentinel::getUser())){
                                ?>
                            </a>
                            <?php
                            }
                            ?>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <?php
                                if(!empty(Sentinel::getUser())){
                                ?>
                                <a href="{{ URL::route('lockscreen',Sentinel::getUser()->id) }}">
                                    <?php
                                    }
                                    ?>

                                    <i class="fa fa-angle-double-right"></i>
                                    Lock
                                    <?php
                                    if(!empty(Sentinel::getUser())){
                                    ?>
                                </a>
                                <?php
                                }
                                ?>
                            </div>
                            <div class="pull-right">
                                <a href="{{ URL::to('admin/logout') }}">
                                    <i class="livicon" data-name="sign-out" data-s="18"></i>
                                    Logout
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="left-side sidebar-offcanvas">
        <section class="sidebar ">
            <div class="page-sidebar  sidebar-nav">
                <div class="nav_icons">
                    <ul class="sidebar_threeicons">
                        <!--<li>
                            <a href="{{ URL::to('admin/form_builder') }}">
                                <i class="livicon" data-name="hammer" title="Form Builder 1" data-loop="true"
                                   data-color="#42aaca" data-hc="#42aaca" data-s="25"></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('admin/form_builder2') }}">
                                <i class="livicon" data-name="list-ul" title="Form Builder 2" data-loop="true"
                                   data-color="#e9573f" data-hc="#e9573f" data-s="25"></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('admin/buttonbuilder') }}">
                                <i class="livicon" data-name="vector-square" title="Button Builder" data-loop="true"
                                   data-color="#f6bb42" data-hc="#f6bb42" data-s="25"></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('admin/gridmanager') }}">
                                <i class="livicon" data-name="new-window" title="Page Builder" data-loop="true"
                                   data-color="#37bc9b" data-hc="#37bc9b" data-s="25"></i>
                            </a>
                        </li>-->
                    </ul>
                </div>
                <div class="clearfix"></div>
                <!-- BEGIN SIDEBAR MENU -->
                @include('admin.layouts._left_menu')
                <!-- END SIDEBAR MENU -->
            </div>
        </section>
    </aside>
    <aside class="right-side">

        <!-- Notifications -->
        @include('notifications')

                <!-- Content -->
        @yield('content')

    </aside>
    <!-- right-side -->
</div>
<a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top"
   data-toggle="tooltip" data-placement="left">
    <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
</a>
<!-- global js -->
<script src="{{ asset('assets/js/app.js') }}" type="text/javascript"></script>


<!-- end of global js -->
<!-- begin page level js -->
@yield('footer_scripts')
        <!-- end page level js -->
</body>
</html>
