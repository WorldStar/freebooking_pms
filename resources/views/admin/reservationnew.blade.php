<html>
<body style="margin-top: 50px;">
                <!-- Main content -->
                    <script src="{{ url('js/html5shiv.js') }}" type="text/javascript"></script>
                <script src="{{ url('js/respond.min.js') }}" type="text/javascript"></script>
                <script src="{{ asset('assets/js/jquery.min.js') }}" type="text/javascript"></script>
                <script src="{{ asset('assets/js/jquery-ui.min.js') }}" type="text/javascript"></script>
                <script src="{{ url('js/daypilot/daypilot-all.min.js') }}" type="text/javascript"></script>
                <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet" type="text/css"/>
                <script src="{{ asset('assets/js/app.js') }}" type="text/javascript"></script>
                <script>
                    /* $.ajaxSetup({
                     headers: {
                     'X-CSRF-TOKEN': "{{ csrf_token() }}"
                     }
                     }); */

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                </script>
                    <?php
                    // check the input
                    //is_numeric($_GET['id']) or die("invalid URL");


                    $start = $_GET['start']; // TODO parse and format
                    $end = $_GET['end']; // TODO parse and format
                    ?>
                    <form id="f" action="backend_create.php" style="padding:20px;">
                        <h1>New Reservation</h1>
                        <div>Name: </div>
                        <div><input type="text" id="name" name="name" value="" /></div>
                        <div>Start:</div>
                        <div><input type="text" id="start" name="start" value="<?php echo $start ?>" /></div>
                        <div>End:</div>
                        <div><input type="text" id="end" name="end" value="<?php echo $end ?>" /></div>
                        <div>Room:</div>
                        <div>
                            <select id="room" name="room">
                                <?php
                                foreach ($rooms as $room) {
                                    $selected = $_GET['resource'] == $room->id ? ' selected="selected"' : '';
                                    $id = $room->id;
                                    $name = $room->name;
                                    print "<option value='$id' $selected>$name</option>";
                                }
                                ?>
                            </select>

                        </div>
                        <div class="space"><input type="submit" value="Save" /> <a href="javascript:close();">Cancel</a></div>
                    </form>

                    <script type="text/javascript">
                        function close(result) {
                            if (parent && parent.DayPilot && parent.DayPilot.ModalStatic) {
                                parent.DayPilot.ModalStatic.close(result);
                            }
                        }

                        $("#f").submit(function () {
                            var f = $("#f");
                            $.post(f.attr("action"), f.serialize(), function (result) {
                                close(eval(result));
                            });
                            return false;
                        });

                        $(document).ready(function () {
                            $("#name").focus();
                            $('.phpdebugbar').css('display', 'none');
                            $('body').css('min-height', 'auto');
                        });

                    </script>
                </body>
                </html>
